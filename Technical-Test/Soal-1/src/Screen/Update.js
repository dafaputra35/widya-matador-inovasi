import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {useState} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';

const Update = props => {
  const [img, setImg] = useState(props.route.params.data.img);
  const [Nama, setNama] = useState(props.route.params.data.Nama);
  const [Panggilan, setPanggilan] = useState(props.route.params.data.Panggilan);
  const [Nomor, setNomor] = useState(props.route.params.data.Nomor);
  const [Email, setEmail] = useState(props.route.params.data.Email);
  const [Jenis, setJenis] = useState(props.route.params.data.Jenis);
  const [Alamat, setAlamat] = useState(props.route.params.data.Alamat);
  const [Pekerjaan, setPekerjaan] = useState(props.route.params.data.Pekerjaan);

  const Buat = {img, Nama, Panggilan, Nomor, Email, Jenis, Alamat, Pekerjaan};
  return (
    <View>
      <View style={styles.container}>
        <Text style={styles.titleInputText}>Nama Lengkap</Text>
        <TextInput
          style={styles.textInput}
          value={Nama}
          onChangeText={value => setNama(value)}
        />

        <Text style={styles.titleInputText}>Nama Panggilan</Text>
        <TextInput
          style={styles.textInput}
          value={Panggilan}
          onChangeText={value => setPanggilan(value)}
        />

        <Text style={styles.titleInputText}>Nomor HP</Text>
        <TextInput
          style={styles.textInput}
          value={Nomor}
          onChangeText={value => setNomor(value)}
        />
        <Text style={styles.titleInputText}>Email</Text>
        <TextInput
          style={styles.textInput}
          value={Email}
          onChangeText={value => setEmail(value)}
        />

        <Text style={styles.titleInputText}>Jenis Kelamin</Text>
        <TextInput
          style={styles.textInput}
          value={Jenis}
          onChangeText={value => setJenis(value)}
        />

        <Text style={styles.titleInputText}>Alamat</Text>
        <TextInput
          style={styles.textInput}
          value={Alamat}
          onChangeText={value => setAlamat(value)}
        />

        <Text style={styles.titleInputText}>Pekerjaan</Text>
        <TextInput
          style={styles.textInput}
          value={Pekerjaan}
          onChangeText={value => setPekerjaan(value)}
        />
      </View>
      <TouchableOpacity
        onPress={() => {
          props.inputdata(Buat);
          props.navigation.navigate('List Data');
        }}
        style={styles.button}>
        <Text
          style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 20,
            textAlign: 'center',
          }}>
          Update
        </Text>
      </TouchableOpacity>
    </View>
  );
};
const reduxDispatch = dispatch => ({
  inputdata: a => dispatch({type: 'UPDATE', inputan: a}),
});
export default connect(null, reduxDispatch)(Update);
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
  },
  titleInputText: {
    fontSize: hp('2.2%'),
    color: 'black',
  },
  textInput: {
    height: hp('6'),
    width: wp('90'),
    fontSize: hp('2%'),
    color: 'black',
    backgroundColor: 'white',
    borderRadius: 8,
    paddingHorizontal: 10,
    elevation: 5,
    marginVertical: 5,
  },
  button: {
    alignSelf: 'center',
    marginVertical: hp(5),
    backgroundColor: '#1C6DD0',
    height: hp('7'),
    width: wp('90'),
    justifyContent: 'center',
    borderRadius: 10,
  },
});
