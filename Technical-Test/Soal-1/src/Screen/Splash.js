import React from 'react';
import {StyleSheet, Text, View, Image, StatusBar} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import logo from '../Asset/Image/widya-matador.png';

const SplashScreen = () => {
  return (
    <LinearGradient
      colors={['#1C6DD0', '#1C6DD0', 'black']}
      locations={[0, 0.65, 5.02]}
      style={{width: wp('100%'), height: hp('100%')}}>
      <StatusBar  
      backgroundColor = "#1C6DD0"  
      barStyle = "dark-content"   
      />  
      <View style={styles.container}>
        <Image source={logo} style={styles.logo} />
      </View>
    </LinearGradient>
  );
};

export default SplashScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: wp('50%'),
    height: hp('30%'),
    resizeMode: 'contain',
  },
  
});
