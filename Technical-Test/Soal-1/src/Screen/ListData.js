import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  StatusBar
} from 'react-native';
import React, {useEffect} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Awesome from 'react-native-vector-icons/FontAwesome5';
import Plus from 'react-native-vector-icons/AntDesign';
import {connect} from 'react-redux';

const ListData = props => {
  const renderItem = ({item}) => {
    console.log(item, 'ini pertama');
    return (
      <TouchableOpacity
        onPress={() => props.navigation.navigate('Detail User', {data: item})}>
        <View style={styles.Card}>
          <Image source={{uri: item.img}} style={styles.icon}></Image>
          <View style={styles.cardText}>
            <Text style={{color: 'black', fontWeight: 'bold'}}>
              {item.Nama}
            </Text>
            <Text style={{color: 'black'}}>{item.Nomor}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  console.log(props.kumpulan);
  return (
    <View style={styles.Container}>
      <StatusBar  
      backgroundColor = "#1C6DD0"  
      barStyle = "dark-content"   
      /> 
      <FlatList data={props.kumpulan} renderItem={renderItem}></FlatList>

      <View style={styles.button}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate('Create User')}>
          <Plus name="pluscircle" size={70} color={'#1C6DD0'}></Plus>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const reduxState = state => {
  console.log(state);
  return {
    kumpulan: state.data.data,
  };
};
export default connect(reduxState, null)(ListData);
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20,
  },

  Card: {
    width: wp('90'),
    height: hp('10'),
    borderRadius: 10,
    borderColor: '#1C6DD0',
    borderWidth: 2,
    backgroundColor: 'white',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 10,
  },

  icon: {
    height: hp('8'),
    width: wp('15'),
    borderRadius: 30,
    marginHorizontal: wp('2'),
    flexDirection: 'row',
  },

  button: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  },
});
