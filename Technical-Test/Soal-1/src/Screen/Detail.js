import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  StatusBar
} from 'react-native';
import React from 'react';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const Detail = props => {
  console.log(props.route.params.data);
  return (
    <View>
      <View style={styles.container}>
      <StatusBar  
      backgroundColor = "#1C6DD0"  
      barStyle = "dark-content"   
      /> 
        <Text style={styles.titleInputText}>Nama Lengkap</Text>
        <TextInput
          style={styles.textInput}
          placeholder={props.route.params.data.Nama}
          editable={false}
        />

        <Text style={styles.titleInputText}>Nama Panggilan</Text>
        <TextInput
          style={styles.textInput}
          placeholder={props.route.params.data.Panggilan}
          editable={false}
        />

        <Text style={styles.titleInputText}>Nomor HP</Text>
        <TextInput
          style={styles.textInput}
          placeholder={props.route.params.data.Nomor}
          editable={false}
        />

        <Text style={styles.titleInputText}>Email</Text>
        <TextInput
          style={styles.textInput}
          placeholder={props.route.params.data.Email}
          editable={false}
        />

        <Text style={styles.titleInputText}>Jenis Kelamin</Text>
        <TextInput
          style={styles.textInput}
          placeholder={props.route.params.data.Jenis}
          editable={false}
        />

        <Text style={styles.titleInputText}>Alamat</Text>
        <TextInput
          style={styles.textInput}
          placeholder={props.route.params.data.Alamat}
          editable={false}
        />

        <Text style={styles.titleInputText}>Pekerjaan</Text>
        <TextInput
          style={styles.textInput}
          placeholder={props.route.params.data.Pekerjaan}
          editable={false}
        />
      </View>
      <TouchableOpacity
        onPress={() =>
          props.navigation.navigate('Update User', {
            data: props.route.params.data,
          })
        }
        style={styles.button}>
        <Text
          style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 20,
            textAlign: 'center',
          }}>
          Edit
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Detail;
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
  },
  titleInputText: {
    fontSize: hp('2.2%'),
    color: 'black',
  },
  textInput: {
    height: hp('6'),
    width: wp('90'),
    fontSize: hp('2%'),
    color: 'black',
    backgroundColor: '#FFF8F3',
    borderRadius: 8,
    paddingHorizontal: 10,
    elevation: 5,
    marginVertical: 5,
  },
  button: {
    alignSelf: 'center',
    marginVertical: hp(5),
    backgroundColor: '#1C6DD0',
    height: hp('7'),
    width: wp('90'),
    justifyContent: 'center',
    borderRadius: 10,
  },
});
