import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Image,
  StatusBar
} from 'react-native';
import React, {useState} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import Feather from 'react-native-vector-icons/Feather';
import {launchImageLibrary} from 'react-native-image-picker';
import gambar from '../Asset/Image/widya-matador.png';

const Create = props => {
  const [img, setImg] = useState();
  const [rawImage, setRawImage] = useState();
  const [Nama, setNama] = useState();
  const [Panggilan, setPanggilan] = useState();
  const [Nomor, setNomor] = useState();
  const [Email, setEmail] = useState();
  const [Jenis, setJenis] = useState();
  const [Alamat, setAlamat] = useState();
  const [Pekerjaan, setPekerjaan] = useState();

  const Buat = {img, Nama, Panggilan, Nomor, Email, Jenis, Alamat, Pekerjaan};

  const option = {
    title: 'Select Poster',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  function pickImage() {
    launchImageLibrary(option, response => {
      if (!response.didCancel) {
        const source = {
          uri: response.assets[0].uri,
          type: response.assets[0].type,
          name: response.assets[0].fileName,
        };
        console.log(source);
        setImg(response.assets[0].uri);
        setRawImage(source);
      } else {
        console.log(response);
      }
    });
  }
  const Datae = () => {
    if (Email == null){
      alert('Email Kosong, Harap Isi Dahulu!')
    }else {
      props.inputdata(Buat)
      props.navigation.navigate('List Data')} 
}
  return (
    <View>
      <ScrollView>
        <View style={styles.container}>
        <StatusBar  
        backgroundColor = "#1C6DD0"  
        barStyle = "dark-content"   
        /> 
          {!img ? (
            <View style={styles.posterContainer}>
              <TouchableOpacity onPress={() => pickImage()}>
                <Feather name="plus-circle" style={styles.buttonImage} />
              </TouchableOpacity>
              <Text style={styles.buttonText}>Add Header Photo</Text>
            </View>
          ) : (
            <View style={styles.posterContainer}>
              <Image
                source={{uri: img}}
                style={styles.poster}
              />
            </View>
          )}
          {img ? (
            <TouchableOpacity
              style={styles.changePosterContainer}
              onPress={() => pickImage()}>
              <Text style={styles.changePosterText}>Change Header Image</Text>
            </TouchableOpacity>
          ) : null}
          <Text style={styles.titleInputText}>Nama Lengkap</Text>
          <TextInput
            onChangeText={e => setNama(e)}
            value={Nama}
            style={styles.textInput}
            placeholder="Nama Lengkap"
          />

          <Text style={styles.titleInputText}>Nama Panggilan</Text>
          <TextInput
            onChangeText={e => setPanggilan(e)}
            value={Panggilan}
            style={styles.textInput}
            placeholder="Panggilan"
          />

          <Text style={styles.titleInputText}>Nomor HP</Text>
          <TextInput
            onChangeText={e => setNomor(e)}
            value={Nomor}
            style={styles.textInput}
            placeholder="Nomor Handphone"
          />

          <Text style={styles.titleInputText}>Email</Text>
          <TextInput
            onChangeText={e => setEmail(e)}
            value={Email}
            style={styles.textInput}
            placeholder="Email"
          />

          <Text style={styles.titleInputText}>Jenis Kelamin</Text>
          <TextInput
            onChangeText={e => setJenis(e)}
            value={Jenis}
            style={styles.textInput}
            placeholder="Jenis Kelamin"
          />

          <Text style={styles.titleInputText}>Alamat</Text>
          <TextInput
            onChangeText={e => setAlamat(e)}
            value={Alamat}
            style={styles.textInput}
            placeholder="Alamat"
          />

          <Text style={styles.titleInputText}>Pekerjaan</Text>
          <TextInput
            onChangeText={e => setPekerjaan(e)}
            value={Pekerjaan}
            style={styles.textInput}
            placeholder="Pekerjaan"
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            Datae()
          }}
          style={styles.button}>
          <Text
            style={{
              color: 'white',
              fontWeight: 'bold',
              fontSize: 20,
              textAlign: 'center',
            }}>
            Create
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const reduxDispatch = dispatch => ({
  inputdata: a => dispatch({type: 'INPUT', inputan: a}),
});
export default connect(null, reduxDispatch)(Create);
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
  },
  posterContainer: {
    width: wp('90%'),
    height: hp('30%'),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: '#E1E0E0',
    marginBottom: 15,
    overflow: 'hidden',
  },
  poster: {
    width: wp('90%'),
    height: hp('30%'),
    resizeMode: 'center',
  },
  buttonImage: {
    fontSize: hp('5%'),
    color: '#9F9F9F',
    marginBottom: 10,
  },
  titleInputText: {
    fontSize: hp('2.2%'),
    color: 'black',
  },
  textInput: {
    height: hp('6'),
    width: wp('90'),
    fontSize: hp('2%'),
    color: 'black',
    backgroundColor: 'white',
    borderRadius: 8,
    paddingHorizontal: 10,
    elevation: 5,
    marginVertical: 5,
  },
  button: {
    alignSelf: 'center',
    marginVertical: hp(5),
    backgroundColor: '#1C6DD0',
    height: hp('7'),
    width: wp('90'),
    justifyContent: 'center',
    borderRadius: 10,
  },
});
