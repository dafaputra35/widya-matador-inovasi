import Update from '../../Screen/Update';

const initialState = {
  data: [],
};
const datae = (state = initialState, action) => {
  switch (action.type) {
    case 'INPUT':
      return {data: [...state.data, action.inputan]};

    case 'UPDATE':
      const update = [...state.data].map(e => {
        return e.Email === action.inputan.Email? action.inputan : e
      });
      return {data: update};

    default:
      return state;
  }
};
export default datae;
