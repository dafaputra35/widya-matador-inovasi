import {createStore, applyMiddleware} from 'redux';
import rootReducer from './Reducers/index';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './Saga/index';

const sagaMiddleware = createSagaMiddleware();

// 1. Membuat Store
const storeRedux = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default storeRedux;
