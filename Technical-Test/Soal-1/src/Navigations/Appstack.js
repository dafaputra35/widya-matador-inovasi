import React, {useState, useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Create from '../Screen/Create';
import Detail from '../Screen/Detail';
import ListData from '../Screen/ListData';
import Update from '../Screen/Update';
import SplashScreen from '../Screen/Splash';

const Stack = createStackNavigator();

const Appstack = () => {
  const [load, setLoad] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoad(false);
    }, 3000);
  }, []);

  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: 'white',
        headerTitleAlign: 'center',
        headerStyle: {backgroundColor: '#1C6DD0'},
      }}>
      {load ? (
        <Stack.Screen
          options={{headerShown: false}}
          name="Splash Screen"
          component={
            SplashScreen}/>
      ) : (
        <>
          <Stack.Screen name="List Data" component={ListData}/>
          <Stack.Screen name="Detail User" component={Detail}/>
          <Stack.Screen name="Create User" component={Create}/>
          <Stack.Screen name="Update User" component={Update}/>
        </>
      )}
    </Stack.Navigator>
  );
};

export default Appstack;
