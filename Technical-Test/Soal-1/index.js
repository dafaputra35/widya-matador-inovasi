/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Detail from './src/Screen/Detail';

AppRegistry.registerComponent(appName, () => App);
