import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import AppStack from './src/Navigations/Appstack';
import {Provider} from 'react-redux';
import storeRedux from './src/Redux/Store';

const App = () => {
  return (
    <Provider store={storeRedux}>
      <NavigationContainer>
        <AppStack></AppStack>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
